/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.storeproject.poc;
import model.Customer;
import model.User;
import model.Product;
import model.Receipt;

/**
 *
 * @author Thanyarak Namwong
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Chayen", 30);
        Product p2 = new Product(2,"pink milk", 40);
        User seller = new User("cream", "no112", "password");
        Customer customer = new Customer("bam", "007");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
    }
}

                
  
